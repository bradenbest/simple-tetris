## Simple Tetris

I wrote a simple tetris game with ncurses. Ncurses is used primarily for handling input, and secondarily for drawing, since it's there and all...

## Instructions

### Compile

Just run

    $ make

Make sure you have `make` and a `c compiler` installed. Do not compile this with a C++ compiler. C++ is not C.

To configure the game, look at `makefile` and `config.mk`

### Install

    $ sudo make install
    $ sudo make unintall

### Play

Assuming you're running it as `tetris`...

    $ tetris

You can also run it with flags to change the behavior

    $ tetris -h # lists all of the options

### Controls

By default, there are three control schemes.

Scheme A (standard):

    Left/Right:           Left/Right
    Drop (Hard/Soft):     Up/Down
    Rotate (Left/Right):  z/x
    Hold Piece:           c
    Pause:                p
    Quit:                 q
    Configure Controls:   shift + c

Scheme B (vi-like):

    Left/Right:           h/l
    Drop (Hard/Soft):     k/j
    Rotate (Left/Right):  g/;
    Hold Piece:           f
    Pause:                p
    Quit:                 q
    Configure Controls:   shift + c

Scheme C (ergonomic):

    Left/Right:           a/d
    Drop (Hard/Soft):     w/s
    Rotate (Left/Right):  [/]
    Hold Piece:           \
    Pause:                p
    Quit:                 q
    Configure Controls:   shift + c

Additionally, a "custom" scheme is selectable whereby you can define your own controls.

The controls are not remembered. You must define your custom controls every time you run the program.

If you want your controls to be permanent, to add them, edit `keymap.h`, `control-scheme.c`, `control-scheme.h`, and `controls.c`.

Particularly, you'll need to do a few specific things:

1. Make room for your scheme
2. Make it possible to switch to said scheme
3. Define the scheme

To make room for the scheme, simply change the array length of keys in `keymap.h`

    int keys[4]; -> int keys[5];

To make it possible for the program to recognize your scheme, add an identifier to `control-scheme.h`

    enum control_scheme_id {
        csid_a,
        csid_b,
        csid_c,
        csid_my_scheme, // like this
        csid_custom,
        ...
    };

And then add the string that the user must type to activate it in `control-scheme.c`

    static const char *control_scheme_names[csid_end] = {
        "A",
        "B",
        "C",
        "MY SCHEME", // like this
        "CUSTOM"
    };

And finally, to define the scheme, just follow the pattern in `controls.c`

    static struct keymap controls_map[] = {
        { { KEY_LEFT, 'h', 'a', /* Your key goes here */ }, ... },
        ...
    };

Make sure that it positionally matches up with where you put the `enum` and the `string`, otherwise, the user entering `my scheme` into the prompt will result in something else being selected. Perhaps the custom scheme.

Also, it'd be a good idea to use something short and without spaces for the user's convenience

## License

Like a lot of my software, this is public domain. So you can do anything you want with the software without fear of a DMCA or legal action from me. Likewise, I'm not liable if you get anyone's leg shot off in a horrible line editing accident. I'm also immune if you try to be cute and license a redistribution of this under a different license and then try to sue me using that license.

(Although if you make money off of it and want to send me a check, I won't stop you ;) )
