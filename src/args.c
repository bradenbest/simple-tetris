#include "args.h"
#include "main.h"

#include <string.h>
#include <stddef.h>

static struct argmap argmap[] = {
    { "h",  "Show this help",                          cmd_help, 0 },
    { "c",  "Change Control Scheme {A|B|C|Custom}",    cmd_change_control_scheme, 1 },
    { "m",  "set mode {endless|limited}",              cmd_set_mode, 1 },
    { "i",  "invisible mode, x2 score bonus per turn", cmd_set_invisible_mode, 0 },

    { .end = 1 }
};

struct argmap *
args_find(char *key)
{
    for(struct argmap *selmap = args_start(); !args_end(selmap); selmap = args_next(selmap))
        if(!strcmp(selmap->key, key))
            return selmap;

    return NULL;
}

struct argmap *
args_start(void)
{
    return argmap;
}

struct argmap *
args_next(struct argmap *selmap)
{
    return selmap + 1;
}

int
args_end(struct argmap *selmap)
{
    return selmap->end;
}

