#ifndef ARGS_H
#define ARGS_H

#include <stddef.h>

struct argmap {
    char *key;
    char *description;
    void (*callback)(char **);
    size_t nparams;

    int end;
};

struct argmap  *  args_find  (char *key);
struct argmap  *  args_start (void);
struct argmap  *  args_next  (struct argmap *);
int               args_end   (struct argmap *);

#endif
