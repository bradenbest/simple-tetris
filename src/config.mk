DISABLED_WARNINGS = \
	-Wno-missing-field-initializers \
	-Wno-unused-parameter 

DEFS = \
    -DVERSION='"1.0.0"' \
	-DSTAGE_WIDTH='10' \
	-DSTAGE_HEIGHT='22' \
	-DNHIDDEN_ROWS='2' \
	-DFRAMES_PER_SECOND='32'

FLAGS = -std=gnu99 -pedantic -Wall -Wextra -O2 -g $(DEFS) $(DISABLED_WARNINGS)
LIBS = -lncurses

# -Wno-missing-field-initializers: two reasons why I'm not worried about this. 
#
# Reason #1: the standard guarantees that fields (of a union, struct or array) are zero if one or more of the following conditions are met:
#
#     1. it is external (i.e. global)
#     2. it is static
#     3. at least one initializer is given
#
# The "variables" in question meet all three of these.
#
# Reason #2: The objects in question are defined in an easy-to-read table with a declarative programming style (controls.c).

# -Wno-unused-parameters: The parameters in question are explicitly named "unused" to indicate that I never intended to use them. 'Nuff said.
