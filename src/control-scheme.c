#include "control-scheme.h"
#include "controls.h"
#include "draw.h"
#include "io.h"
#include "util.h"

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <ncurses.h>

enum control_scheme_id control_scheme = csid_a;

static int custom_defined = 0;

static const char *control_scheme_names[csid_end] = {
    "A",
    "B",
    "C",
    "CUSTOM"
};

static size_t   get_description_fmt_width  (void);
static void     show_schemes_header        (void);
static void     show_control               (struct keymap *);
static char  *  get_ctrl_str               (int ch);
static void     define_custom              (void);
static void     sanitize                   (char *);

enum control_scheme_id
control_scheme_find(char *buf)
{
    sanitize(buf);

    for(enum control_scheme_id csid = csid_a; csid < csid_end; csid++)
        if(!strcmp(control_scheme_names[csid], buf))
            return csid;

    return csid_invalid;
}

int
control_scheme_set(enum control_scheme_id csid)
{
    if(csid == csid_invalid)
        return 0;
    
    if(csid == csid_custom)
        define_custom();

    control_scheme = csid;

    return 1;
}

void
control_scheme_show_schemes(void)
{
    show_schemes_header();

    for(struct keymap *selmap = controls_start(); !controls_end(selmap); selmap = controls_next(selmap))
        show_control(selmap);
}

void
control_scheme_show_scheme(int scheme)
{
    size_t dwidth = get_description_fmt_width();

    for(struct keymap *selmap = controls_start(); !controls_end(selmap); selmap = controls_next(selmap))
        printf("  %*s: %s\n", (int)dwidth, selmap->description, get_ctrl_str(selmap->keys[scheme]));
}

static size_t
get_description_fmt_width(void)
{
    size_t longest_length = 0;

    for(struct keymap *selmap = controls_start(); !controls_end(selmap); selmap = controls_next(selmap))
        longest_length = MAX(strlen(selmap->description), longest_length);

    return longest_length;
}

static void
show_control(struct keymap *selmap)
{
    size_t dwidth = get_description_fmt_width();

    printf("%*s  ", (int)dwidth, selmap->description);

    for(enum control_scheme_id csid = csid_a; csid < csid_end; csid++)
        printf("%-6s  ", get_ctrl_str(selmap->keys[csid]));

    printf("\n");
}

static void
show_schemes_header(void)
{
    size_t dwidth = get_description_fmt_width();

    printf("%*s  ", (int)dwidth, "Scheme");

    for(enum control_scheme_id csid = 0; csid < csid_end; csid++)
        printf("%-6s  ", control_scheme_names[csid]);

    printf("\n\n");
}

static char *
get_ctrl_str(int ch)
{
    const char *key_strings[KEY_MAX] = {
        [0]         = "None",
        [KEY_DOWN]  = "Down",
        [KEY_UP]    = "Up",
        [KEY_LEFT]  = "Left",
        [KEY_RIGHT] = "Right",
    };

    static char chbuf[2] = "";
    static char keyidbuf[7] = "0x";

    *chbuf = ch;

    if(ch > 0x20 && ch < 0x7f)
        return chbuf;

    if(key_strings[ch] != NULL)
        return (char *)key_strings[ch];

    sprintf(keyidbuf + 2, "%04x", ch);
    return keyidbuf;

}

static void
define_custom(void)
{
    if(custom_defined)
        return;

    draw_init();

    for(struct keymap *selmap = controls_start(); !controls_end(selmap); selmap = controls_next(selmap)){
        draw_cs_key_prompt(selmap->description);
        selmap->keys[csid_custom] = io_get_char();
    }

    draw_finish();
    custom_defined = 1;
}

static void
sanitize(char *str)
{
    while(*str){
        if(strchr("\r\n", *str))
            *str = '\0';

        if(*str >= 'a' && *str <= 'z')
            *str -= 32;

        str++;
    }
}
