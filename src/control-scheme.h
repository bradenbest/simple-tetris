#ifndef CONTROL_SCHEME_H
#define CONTROL_SCHEME_H

enum control_scheme_id {
    csid_a,
    csid_b,
    csid_c,
    csid_custom,

    csid_end,
    csid_invalid
};

extern enum control_scheme_id control_scheme;

enum control_scheme_id     control_scheme_find          (char *buf);
int                        control_scheme_set           (enum control_scheme_id csid);
void                       control_scheme_show_schemes  (void);
void                       control_scheme_show_scheme   (int scheme);

#endif
