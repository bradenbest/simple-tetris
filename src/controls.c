#include "controls.h"
#include "keymap.h"
#include "tetromino.h"
#include "hold-bag.h"
#include "game.h"

#include <stddef.h>
#include <ncurses.h>

static struct keymap controls_map[] = {
    { { KEY_LEFT,  'h', 'a'  }, "Left",               tetromino_move_left          },
    { { KEY_RIGHT, 'l', 'd'  }, "Right",              tetromino_move_right         },
    { { KEY_DOWN,  'j', 's'  }, "Soft Drop",          tetromino_move_down          },
    { { KEY_UP,    'k', 'w'  }, "Hard Drop",          tetromino_slam_down          },
    { { 'z',       'g', '['  }, "Rotate Left",        tetromino_rotate_l           },
    { { 'x',       ';', ']'  }, "Rotate Right",       tetromino_rotate_r           },
    { { 'c',       'f', '\\' }, "Hold",               hold_bag_swap                },
    { { 'p',       'p', 'p'  }, "Pause",              game_pause                   },
    { { 'q',       'q', 'q'  }, "Quit",               NULL                         },
    { { 'C',       'C', 'C'  }, "Configure Controls", keymap_change_control_scheme },

    { .end = 1 }
};

struct keymap *
controls_start(void)
{
    return controls_map;
}

struct keymap *
controls_next(struct keymap *selmap)
{
    return selmap + 1;
}

int
controls_end(struct keymap *selmap)
{
    return selmap->end;
}
