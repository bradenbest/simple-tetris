#ifndef CONTROLS_H
#define CONTROLS_H

#include "keymap.h"

struct keymap  *  controls_start  (void);
struct keymap  *  controls_next   (struct keymap *);
int               controls_end    (struct keymap *);

#endif
