#include "coord.h"

void
coord_set(struct coord *coord, int x, int y)
{
    coord->x = x;
    coord->y = y;
}
