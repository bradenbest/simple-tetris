#ifndef COORD_H
#define COORD_H

struct coord {
    int x;
    int y;
};

void  coord_set  (struct coord *, int x, int y);

#endif
