#ifndef DEFS_H
#define DEFS_H

#ifndef STAGE_WIDTH
#    define STAGE_WIDTH 10
#endif

#ifndef STAGE_HEIGHT
#    define STAGE_HEIGHT 22
#endif

#ifndef NHIDDEN_ROWS
#    define NHIDDEN_ROWS 2
#endif

#ifndef FRAMES_PER_SECOND
#    define FRAMES_PER_SECOND 32
#endif

#endif
