#include "draw.h"
#include "window.h"
#include "coord.h"
#include "tetromino.h"
#include "shapes.h"
#include "hold-bag.h"
#include "ground.h"
#include "defs.h"

#include <string.h>
#include <stdbool.h>
#include <ncurses.h>

enum {
    w_playfield,
    w_holdbag,
    w_nextbag,
    w_score,
    w_pause,
    w_end
};

struct draw_constants_t {
    char  *  border_text;
    int      border_color;
    char  *  pause_text;
    char  *  shadow_text;
};

struct draw_state_t {
    bool           screen_bump;
    bool           ncurses_running;
    bool           invisible_mode;
    int            last_color;
    struct window  windows[w_end];
};

static const struct draw_constants_t draw_constants = {
    .border_text = "||",
    .border_color = color_white,
    .pause_text  = "PAUSED",
    .shadow_text = "[]"
};

static struct draw_state_t draw_state = { 
    .last_color = -1,
    .windows = {
        [w_playfield] = {
            .pos    = {1, 1},
            .size   = {STAGE_WIDTH, STAGE_HEIGHT - NHIDDEN_ROWS},
            .border = true
        },
        [w_holdbag] = {
            .pos    = {STAGE_WIDTH + 4, 1},
            .size   = {5, 5},
            .title  = "HOLD",
            .border = true
        },
        [w_nextbag] = {
            .pos    = {STAGE_WIDTH + 4, 9},
            .size   = {5, 5},
            .title  = "NEXT",
            .border = true
        },
        [w_score] = {
            .pos    = {1, STAGE_HEIGHT + 2},
            .size   = {STAGE_WIDTH, 3},
            .border = true
        },
        [w_pause] = {
            .pos = {1, STAGE_HEIGHT / 2},
            .size = {STAGE_WIDTH, 1},
        }
    }
};

static void  render_color_cell       (struct window *, int x, int y, int color, char *text);
static void  render_window_border    (struct window *, int color);
static void  render_borders          (void);
static void  render_basic_tetromino  (struct window *, struct tetromino *, char *text);
static void  render_tetromino        (struct tetromino *, char *text);
static void  render_shadow           (struct tetromino *);
static void  render_next             (struct tetromino *next);
static void  render_hold_bag         (struct tetromino *hold_bag);
static void  render_ground           (void);
static void  render_spaced_text      (char *);
static void  apply_bump              (void);

void
draw_init(void)
{
    if(draw_state.ncurses_running)
        return;

    initscr();
    raw();
    noecho();
    curs_set(0);
    keypad(stdscr, TRUE);
    timeout(0);

    start_color();
    init_pair(1, COLOR_WHITE,   COLOR_WHITE);
    init_pair(2, COLOR_CYAN,    COLOR_CYAN);
    init_pair(3, COLOR_GREEN,   COLOR_GREEN);
    init_pair(4, COLOR_RED,     COLOR_RED);
    init_pair(5, COLOR_BLUE,    COLOR_BLUE);
    init_pair(6, COLOR_MAGENTA, COLOR_MAGENTA);
    init_pair(7, COLOR_YELLOW,  COLOR_YELLOW);
    init_pair(8, COLOR_BLACK,   COLOR_BLACK);
    init_pair(9, COLOR_BLACK,   COLOR_WHITE);

    draw_state.ncurses_running = true;
}

void
draw_finish(void)
{
    if(!draw_state.ncurses_running)
        return;

    endwin();
    draw_state.ncurses_running = false;
}

void
draw_set_invisible_mode(bool mode)
{
    draw_state.invisible_mode = mode;
}

bool
draw_get_invisible_mode(void)
{
    return draw_state.invisible_mode;
}

void
draw_pause_screen(void)
{
    struct window *pausewin = draw_state.windows + w_pause;

    clear();
    render_window_border(draw_state.windows + w_playfield, 1);
    window_move_cursor(pausewin, (STAGE_WIDTH - strlen(draw_constants.pause_text)) / 2, 0);
    render_spaced_text(draw_constants.pause_text);
    refresh();
}

void
draw_reset_color(void)
{
    int *lc = &draw_state.last_color;

    if(*lc == -1)
        return;

    attroff(COLOR_PAIR(*lc));
    *lc = -1;
}

void
draw_set_color(int color)
{
    draw_reset_color();
    attron(COLOR_PAIR(color));
    draw_state.last_color = color;
}

void
draw_render_stage(struct tetromino *piece, struct tetromino *next)
{
    clear();

    render_ground();
    render_shadow(piece);

    if(!draw_state.invisible_mode){
        render_borders();
        render_tetromino(piece, draw_constants.border_text);
        render_hold_bag(hold_bag_get_reference());
        render_next(next);
    }

    else {
        render_window_border(draw_state.windows + w_playfield, color_magenta);
        render_window_border(draw_state.windows + w_score, color_magenta);
    }

    refresh();
    draw_remove_screen_bump();
}

void
draw_render_score(long unsigned score, int lines, int frameno)
{
    struct window *scorewin = draw_state.windows + w_score;
    double seconds = (double)frameno / FRAMES_PER_SECOND;

    window_move_cursor(scorewin, 0, 0);
    printw("Score: %lu", score);

    window_move_cursor(scorewin, 0, 1);
    printw("Lines: %i", lines);

    window_move_cursor(scorewin, 0, 2);
    printw("Time:  %0.2fs", seconds);

    refresh();
}


void
draw_add_screen_bump(void)
{
    if(draw_state.screen_bump)
        return;

    draw_state.screen_bump = true;
    apply_bump();
}

void
draw_remove_screen_bump(void)
{
    if(!draw_state.screen_bump)
        return;

    draw_state.screen_bump = false;
    apply_bump();
}

void
draw_cs_key_prompt(char *prompt)
{
    clear();
    mvprintw(0, 0, "Press key for `%s`: ", prompt);
    refresh();
}

static void
render_color_cell(struct window *window, int x, int y, int color, char *text)
{
    window_move_cursor(window, x, y);
    draw_set_color(color);
    addstr(text);
    draw_reset_color();
}

static void
render_window_border(struct window *window, int color)
{
    if(!window->border)
        return;

    draw_set_color(color);
    window_move_cursor(window, -1, -1);

    for(int i = 0; i < window->size.x + 2; i++)
        addstr(draw_constants.border_text);

    window_move_cursor(window, -1, window->size.y);

    for(int i = 0; i < window->size.x + 2; i++)
        addstr(draw_constants.border_text);

    for(int y = 0; y <= window->size.y; y++){
        window_move_cursor(window, -1, y);
        addstr(draw_constants.border_text);
        window_move_cursor(window, window->size.x, y);
        addstr(draw_constants.border_text);
    }

    if(window->title != NULL){
        draw_set_color(9);
        window_move_cursor(window, (window->size.x - strlen(window->title)) / 2, -1);
        render_spaced_text(window->title);
    }

    draw_reset_color();
}

static void
render_borders(void)
{
    for(int i = 0; i < w_end; i++)
        render_window_border(draw_state.windows + i, 1);
}

static void
render_basic_tetromino(struct window *window, struct tetromino *piece, char *text)
{
    struct coord *selsprite;

    for(int i = 0; i < 4; i++){
        selsprite = piece->sprite + i;
        render_color_cell(window, selsprite->x, selsprite->y, piece->color, text);
    }
}

static void
render_tetromino(struct tetromino *piece, char *text)
{
    struct window *playwin = draw_state.windows + w_playfield;
    struct coord *positions = tetromino_get_absolute_position(piece);

    for(int i = 0; i < 4; i++)
        if(positions[i].y >= NHIDDEN_ROWS)
            render_color_cell(playwin, positions[i].x, positions[i].y - NHIDDEN_ROWS, piece->color, text);
}

static void
render_shadow(struct tetromino *piece)
{
    struct tetromino shadow;

    memcpy(&shadow, piece, sizeof shadow);
    shadow.color = color_plain;
    tetromino_slam_down(&shadow);
    render_tetromino(&shadow, draw_constants.shadow_text);
}

static void
render_next(struct tetromino *next)
{
    render_basic_tetromino(draw_state.windows + w_nextbag, next, draw_constants.border_text);
}

static void
render_hold_bag(struct tetromino *hold_bag)
{
    if(hold_bag != NULL)
        render_basic_tetromino(draw_state.windows + w_holdbag, hold_bag, "||");
}

static void
render_ground(void)
{
    struct window *playwin = draw_state.windows + w_playfield;
    struct ground *cell;

    for(int y = NHIDDEN_ROWS; y < STAGE_HEIGHT; y++){
        for(int x = 0; x < STAGE_WIDTH; x++){
            cell = ground_get_cell(x, y);

            if(cell->value)
                render_color_cell(playwin, x, y - NHIDDEN_ROWS, cell->value, "  ");
        }
    }
}

static void
render_spaced_text(char *text)
{
    while(*text){
        addch(' ');
        addch(*(text++));
    }
}

static void
apply_bump(void)
{
    for(int i = 0; i < w_end; i++)
        draw_state.windows[i].pos.y += 1 * (draw_state.screen_bump) ? 1 : -1;
}
