#ifndef DRAW_H
#define DRAW_H

#include "tetromino.h"

#include <stdbool.h>

enum color {
    color_plain,
    color_white,
    color_cyan,
    color_green,
    color_red,
    color_blue,
    color_magenta,
    color_yellow,
    color_black,
    color_inverted
};

void  draw_init                (void);
void  draw_finish              (void);
void  draw_set_invisible_mode  (bool);
bool  draw_get_invisible_mode  (void);
void  draw_pause_screen        (void);
void  draw_reset_color         (void);
void  draw_set_color           (int color);
void  draw_render_stage        (struct tetromino *piece, struct tetromino *next);
void  draw_render_score        (long unsigned score, int lines, int frameno);
void  draw_add_screen_bump     (void);
void  draw_remove_screen_bump  (void);
void  draw_cs_key_prompt       (char *prompt);

#endif
