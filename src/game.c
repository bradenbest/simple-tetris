#include "game.h"
#include "defs.h"
#include "tetromino.h"
#include "tetromino-bag.h"
#include "hold-bag.h"
#include "ground.h"
#include "draw.h"
#include "keymap.h"
#include "io.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

enum {
    exit_game_over,
    exit_continue,
    exit_victory
};

struct game_state_t {
    unsigned long       score;
    unsigned int        total_lines_cleared;
    unsigned int        frame;
    unsigned int        fall_speed;
    bool                paused;
    enum marathon_mode  marathon_mode;
};

static struct game_state_t game_state = {
    .total_lines_cleared = 149,
    .fall_speed    = 2,
    .marathon_mode = marathon_endless
};

static int   interpret_user_input  (struct tetromino *piece, int input);
static bool  is_fall_frame         (void);
static void  do_sleep              (void);
static int   simulate_piece        (struct tetromino *, struct tetromino *);
static void  update_score          (void);
static void  update_fall_speed     (void);

int
game_pause(struct tetromino *unused)
{
    game_state.paused = !game_state.paused;

    return 1;
}

int
game_loop(void)
{
    struct tetromino *current;
    struct tetromino *next = tetromino_bag_get();
    int status;

    ground_init();
    draw_init();

    while(1){
        current = next;
        next = tetromino_bag_get();

        if((status = simulate_piece(current, next)) != exit_continue)
            break;
    }

    draw_finish();

    return status;
}

void
game_report_stats(void)
{
    double seconds = (double)game_state.frame / FRAMES_PER_SECOND;
    unsigned long total_score = game_state.score + (long)seconds * 10 + game_state.total_lines_cleared * 20;

    printf("Lines Cleared:     %i\n", game_state.total_lines_cleared);
    printf("Base Score:        %lu\n", game_state.score);
    printf("Total Time Played: %0.2f Seconds\n\n", seconds);
    printf("Final Score: %lu\n", total_score);
}

void
game_set_marathon_mode(enum marathon_mode mode)
{
    game_state.marathon_mode = mode;
}

static int
interpret_user_input(struct tetromino *piece, int input)
{
    struct keymap *match = keymap_find(input);

    if(match == NULL)
        return exit_continue;

    if(match->callback == NULL)
        return exit_game_over;

    match->callback(piece);
    return exit_continue;
}

static bool
is_fall_frame(void)
{
    return game_state.frame % (FRAMES_PER_SECOND / game_state.fall_speed) == 0;
}

static void
do_sleep(void)
{
    usleep(1000000 / FRAMES_PER_SECOND);
}

static int
simulate_piece(struct tetromino *src, struct tetromino *next)
{
    static struct tetromino piece;
    bool landed = false;

    memcpy(&piece, src, sizeof piece);
    tetromino_reset(&piece);

    while(!landed){
        if(interpret_user_input(&piece, io_get_keypress()) == exit_game_over)
            return exit_game_over;

        if(game_state.paused){
            draw_pause_screen();
            continue;
        }

        if(is_fall_frame() && !tetromino_move_down(&piece)){
            if(!ground_add_piece(&piece))
                return exit_game_over;

            landed = true;
        }

        game_state.frame++;
        draw_render_stage(&piece, next);
        draw_render_score(game_state.score, game_state.total_lines_cleared, game_state.frame);
        do_sleep();
    }

    draw_add_screen_bump();
    update_score();
    update_fall_speed();

    if(game_state.marathon_mode == marathon_limited && game_state.total_lines_cleared > 150)
        return exit_victory;

    return exit_continue;
}

static void
update_score(void)
{
    int temp_score = 10;
    int lines = ground_check_lines();

    game_state.total_lines_cleared += lines;

    if(!hold_bag_open())
        temp_score += 20;

    if(lines)
        temp_score += 100;

    while(lines)
        temp_score *= lines--;

    if(draw_get_invisible_mode() == true)
        temp_score *= 2;

    game_state.score += temp_score;
}

static void
update_fall_speed(void)
{
    if(game_state.score > 10000)
        game_state.fall_speed = 4;

    if(game_state.score > 100000)
        game_state.fall_speed = 8;

    if(game_state.score > 1000000)
        game_state.fall_speed = 16;
}
