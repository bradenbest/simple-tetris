#ifndef GAME_H
#define GAME_H

#include "tetromino.h"

enum marathon_mode {
    marathon_endless,
    marathon_limited
};

int   game_pause              (struct tetromino *unused);
int   game_loop               (void);
void  game_report_stats       (void);
void  game_set_marathon_mode  (enum marathon_mode);

#endif
