#include "ground.h"
#include "defs.h"
#include "coord.h"
#include "tetromino.h"
#include "draw.h"

#include <string.h>

static struct ground ground[STAGE_WIDTH * STAGE_HEIGHT];

static int   is_out_of_bounds  (int x, int y);
static int   check_line        (int line);
static void  delete_line       (int line);
static void  drop_column       (int x, int y);
static void  drop_cell         (int x, int y);

void
ground_init(void)
{
    memset(ground, 0, sizeof ground);
}

struct ground *
ground_get_cell(int x, int y)
{
    if(is_out_of_bounds(x, y))
        return NULL;

    return ground + x + (y * STAGE_WIDTH);
}

int
ground_set_cell(int x, int y, int value)
{
    struct ground *cell = ground_get_cell(x, y);

    if(!!cell->value == !!value)
        return 0;

    cell->value = value;

    return 1;
}

int
ground_add_piece(struct tetromino *piece)
{
    struct coord *positions = tetromino_get_absolute_position(piece);

    for(int i = 0; i < 4; i++)
        if(positions[i].y < 0 || !ground_set_cell(positions[i].x, positions[i].y, piece->color))
            return 0;

    return 1;
}

int
ground_collision_check(struct coord *positions)
{
    struct ground *cell;

    for(int i = 0; i < 4; i++)
        if((cell = ground_get_cell(positions[i].x, positions[i].y)) != NULL && cell->value > 0)
            return 1;

    return 0;
}

int
ground_check_lines(void)
{
    int deleted = 0;

    for(int line = 0; line < STAGE_HEIGHT; line++){
        if(check_line(line)){
            delete_line(line);
            deleted++;
        }
    }

    return deleted;
}

static int
is_out_of_bounds(int x, int y)
{
    return x < 0 || y < 0 || x >= STAGE_WIDTH || y >= STAGE_HEIGHT;
}

static int
check_line(int line)
{
    for(int x = 0; x < STAGE_WIDTH; x++)
        if(ground_get_cell(x, line)->value == 0)
            return 0;

    return 1;
}

static void
delete_line(int y)
{
    for(int x = 0; x < STAGE_WIDTH; x++)
        drop_column(x, y);
}

static void
drop_column(int x, int y)
{
    while(y > 0)
        drop_cell(x, y--);

    ground_set_cell(x, 0, 0);
}

static void
drop_cell(int x, int y)
{
    struct ground *bottom = ground_get_cell(x, y);
    struct ground *top    = ground_get_cell(x, y - 1);

    memcpy(bottom, top, sizeof (struct ground));
}
