#ifndef GROUND_H
#define GROUND_H

#include "coord.h"
#include "tetromino.h"

struct ground {
    int value;
    int color;
};

void              ground_init                    (void);
struct ground  *  ground_get_cell                (int x, int y);
int               ground_set_cell                (int x, int y, int value);
int               ground_add_piece               (struct tetromino *);
int               ground_collision_check         (struct coord *positions);
int               ground_check_lines             (void);

#endif
