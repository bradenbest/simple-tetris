#include "hold-bag.h"
#include "tetromino.h"
#include "tetromino-bag.h"

#include <string.h>

static struct tetromino hold_bag = {0};
static int hold_bag_used = 0;
static int hold_bag_empty = 1;

struct tetromino *
hold_bag_get_reference(void)
{
    if(hold_bag_empty)
        return NULL;

    return &hold_bag;
}

int
hold_bag_swap(struct tetromino *piece)
{
    if(hold_bag_used)
        return 0;

    if(hold_bag_empty)
        memcpy(&hold_bag, tetromino_bag_get(), sizeof (struct tetromino));

    tetromino_swap(piece, &hold_bag);
    tetromino_reset(piece);
    tetromino_reset(&hold_bag);
    hold_bag_used = 1;
    hold_bag_empty = 0;

    return 1;
}

int
hold_bag_open(void)
{
    if(hold_bag_used == 0)
        return 0;

    hold_bag_used = 0;
    return 1;
}
