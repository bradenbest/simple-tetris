#ifndef HOLD_BAG_H
#define HOLD_BAG_H

#include "tetromino.h"

struct tetromino  *  hold_bag_get_reference  (void);
int                  hold_bag_swap           (struct tetromino *piece);
int                  hold_bag_open           (void);

#endif
