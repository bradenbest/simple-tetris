#include "io.h"

#include <ncurses.h>

#define NCURSES_STDIN_FD 3

void
io_drain(void)
{
    while(getch() != -1)
        ;
}

int
io_get_keypress(void)
{
    int ch = getch();

    //io_drain();

    return ch;
}

int
io_get_char(void)
{
    int ch;

    timeout(-1);
    ch = getch();
    timeout(0);

    return ch;
}
