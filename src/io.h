#ifndef IO_H
#define IO_H

void  io_drain         (void);
int   io_get_keypress  (void);
int   io_get_char      (void);

#endif
