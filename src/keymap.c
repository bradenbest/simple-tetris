#include "keymap.h"
#include "control-scheme.h"
#include "controls.h"
#include "draw.h"
#include "tetromino.h"

#include <stdio.h>
#include <ncurses.h>
#include <unistd.h>

struct keymap *
keymap_find(int input)
{
    for(struct keymap *selmap = controls_start(); !controls_end(selmap); selmap = controls_next(selmap))
        if(selmap->keys[control_scheme] == input)
            return selmap;

    return NULL;
}

int
keymap_change_control_scheme(struct tetromino *unused)
{
    char buf[16] = "";
    enum control_scheme_id csid = csid_a;
    int retval = 1;

    draw_finish();
    control_scheme_show_schemes();
    printf("\nSelect a control scheme (default: A): ");
    fflush(stdout);

    if(read(0, buf, 15) > 1)
        csid = control_scheme_find(buf);

    printf("\n");

    if(!control_scheme_set(csid)){
        puts("Invalid");
        retval = 0;
    }

    else {
        puts("Selected scheme:");
        control_scheme_show_scheme(csid);
    }

    puts("\nPress Enter to continue.");
    getchar();
    draw_init();

    return retval;
}
