#ifndef KEYMAP_H
#define KEYMAP_H

#include "tetromino.h"

struct keymap {
    int keys[4];
    char *description;
    int (*callback)(struct tetromino *);

    int end;
};

struct keymap  *  keymap_find                   (int input);
int               keymap_change_control_scheme  (struct tetromino *);

#endif
