#include "main.h"
#include "game.h"
#include "keymap.h"
#include "control-scheme.h"
#include "args.h"
#include "draw.h"

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>

#define OPT_CONTROL_SCHEME_SET 1
#define OPT_HELP 2

#define FLAG_SET(flag) optflags |= flag
#define FLAG_GET(flag) (optflags & flag)

static int optflags = 0;

void
cmd_help(char **unused)
{
    printf("tetris v" VERSION " - the game of tetris\n");
    printf("Usage: tetris [options]\n\nOptions:\n");

    for(struct argmap *selmap = args_start(); !args_end(selmap); selmap = args_next(selmap))
        printf("  -%-2s  %s\n", selmap->key, selmap->description);

    FLAG_SET(OPT_HELP);
}

void
cmd_change_control_scheme(char **params)
{
    char *scheme_name = params[0];
    enum control_scheme_id csid = control_scheme_find(scheme_name);

    control_scheme_set(csid);
    FLAG_SET(OPT_CONTROL_SCHEME_SET);
}

void
cmd_set_mode(char **params)
{
    char *mode = params[0];

    if(!strcmp(mode, "endless"))
        game_set_marathon_mode(marathon_endless);

    if(!strcmp(mode, "limited"))
        game_set_marathon_mode(marathon_limited);
}

void
cmd_set_invisible_mode(char **unused)
{
    draw_set_invisible_mode(true);
}

int
parse_opt(char *opt, char **params)
{
    struct argmap *selmap = args_find(opt);

    if(selmap == NULL)
        return -1;

    selmap->callback(params);
    return selmap->nparams;
}

void
parse_args(char **args)
{
    int temp;

    for(char *arg; (arg = *args) != NULL; args++){
        if(arg[0] == '-'){
            if((temp = parse_opt(arg + 1, args + 1)) == -1){
                printf("Unrecognized option `%s`\n", arg);
                exit(1);
            }

            args += temp + 1;
        }

        if(FLAG_GET(OPT_HELP))
            break;
    }
}

int
main(int unused_length_argument, char **argv)
{
    int exit_status;

    parse_args(argv);

    if(FLAG_GET(OPT_HELP))
        return 0;

    if(!FLAG_GET(OPT_CONTROL_SCHEME_SET))
        keymap_change_control_scheme(NULL);

    exit_status = game_loop();

    if(exit_status == 0)
        printf("Game Over.\n\n");
    else
        printf("You Won!\n\n");

    game_report_stats();
    return 0;
}
