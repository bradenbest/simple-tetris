#ifndef MAIN_H
#define MAIN_H

void  cmd_help                   (char **);
void  cmd_change_control_scheme  (char **);
void  cmd_set_mode               (char **);
void  cmd_set_invisible_mode     (char **);

#endif
