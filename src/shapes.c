#include "shapes.h"
#include "tetromino.h"
#include "draw.h"

static const struct tetromino tetrominoes[ts_end] = {
    [ts_o] = {
        .color = color_white,
        .period = 1,
        .sprite = {
            {1, 1}, {2, 1}, {1, 2}, {2, 2}
        }
    },
    [ts_i] = {
        .color = color_cyan,
        .period = 2,
        .sprite = {
            {1, 0}, {1, 1}, {1, 2}, {1, 3},
            {0, 2}, {1, 2}, {2, 2}, {3, 2}
        }
    },
    [ts_s] = {
        .color = color_green,
        .period = 2,
        .sprite = {
            {1, 0}, {1, 1}, {2, 1}, {2, 2},
            {0, 1}, {1, 0}, {1, 1}, {2, 0}
        }
    },
    [ts_z] = {
        .color = color_red,
        .period = 2,
        .sprite = {
            {1, 1}, {1, 2}, {2, 0}, {2, 1},
            {0, 0}, {1, 0}, {1, 1}, {2, 1}
        }
    },
    [ts_l] = {
        .color = color_magenta,
        .period = 4,
        .sprite = {
            {0, 0}, {0, 1}, {0, 2}, {1, 2},
            {0, 0}, {0, 1}, {1, 0}, {2, 0},
            {0, 0}, {1, 0}, {1, 1}, {1, 2},
            {0, 1}, {1, 1}, {2, 1}, {2, 0}
        }
    },
    [ts_j] = {
        .color = color_blue,
        .period = 4,
        .sprite = {
            {0, 2}, {1, 0}, {1, 1}, {1, 2},
            {0, 0}, {0, 1}, {1, 1}, {2, 1},
            {0, 0}, {0, 1}, {0, 2}, {1, 0},
            {0, 0}, {1, 0}, {2, 0}, {2, 1}
        }
    },
    [ts_t] = {
        .color = color_yellow,
        .period = 4,
        .sprite = {
            {0, 0}, {1, 0}, {1, 1}, {2, 0},
            {1, 1}, {2, 0}, {2, 1}, {2, 2},
            {0, 2}, {1, 1}, {1, 2}, {2, 2},
            {0, 0}, {0, 1}, {0, 2}, {1, 1}
        }
    }
};

const struct tetromino *
shapes_get(enum tetromino_shape tsid)
{
    return tetrominoes + tsid;
}
