#ifndef SHAPES_H
#define SHAPES_H

#include "tetromino.h"

enum tetromino_shape {
    ts_o,
    ts_i,
    ts_s,
    ts_z,
    ts_l,
    ts_j,
    ts_t,

    ts_end
};

const struct tetromino  *  shapes_get  (enum tetromino_shape tsid);

#endif
