#include <stdio.h>
#include <stdlib.h>

static int random_bag[7] = {0,1,2,3,4,5,6};

int
random_bag_cmp(const void *a, const void *b)
{
    return rand() % 2;
}

int
reset_random_bag(void)
{
    //qsort(random_bag, 7, sizeof (int), random_bag_cmp);
    for(int temp, r, i = 6; (r = rand() % (i + 1)), i > 0; i--)
        temp = random_bag[i],
        random_bag[i] = random_bag[r],
        random_bag[r] = temp;

}

void
print_random_bag(void)
{
    for(int i = 0; i < 7; i++)
        printf("%i ", random_bag[i]);

    putchar('\n');
}

int main(){
    srand(time(NULL));
    print_random_bag();
    reset_random_bag();
    print_random_bag();
    reset_random_bag();
    print_random_bag();
}
