#include "../tetromino-bag.h"
#include "../shapes.h"

#include <stdio.h>

#define BLACK     0
#define RED       1
#define GREEN     2
#define YELLOW    3
#define BLUE      4
#define MAGENTA   5
#define TURQUOISE 6
#define WHITE     7

static int colors[] = {
    WHITE,     // O
    TURQUOISE, // I
    GREEN,     // S
    RED,       // Z
    BLUE,      // L
    MAGENTA,   // J
    YELLOW     // T
};

static int
test_tetromino_bag_get(void)
{
    return tetromino_bag_get() - shapes_get(0);
}

int
main()
{
    printf("1 line = 1 bag\n");

    for(int i = 0; i < 100; i++){
        for(int j = 0; j < 7; j++)
            printf("\e[1;4%i;30m  \e[0m", colors[test_tetromino_bag_get()]);

        printf("\n");
    }

    return 0;
}
