#include "tetromino-bag.h"
#include "tetromino.h"
#include "shapes.h"

#include <time.h>
#include <string.h>
#include <stdlib.h>

struct tetromino_bag_state {
    unsigned bag[ts_end];
    unsigned index;
    unsigned init;
};

static void  bag_check  (void);
static void  bag_init   (void);
static void  bag_reset  (void);
static void  bag_swap   (int, int);

static struct tetromino_bag_state tb_state;

struct tetromino *
tetromino_bag_get(void)
{
    bag_check();

    return (struct tetromino *)shapes_get(tb_state.bag[tb_state.index++]);
}

static void
bag_check(void)
{
    if(!tb_state.init)
        bag_init();

    if(tb_state.index >= ts_end)
        bag_reset();
}

static void
bag_init(void)
{
    tb_state.init = 1;

    for(int i = 0; i < ts_end; i++)
        tb_state.bag[i] = i;

    srand(time(NULL));
    bag_reset();
}

static void
bag_reset(void)
{
    for(int i = ts_end - 1; i > 0; i--)
        bag_swap(i, rand() % (i + 1));

    tb_state.index = 0;
}

static void
bag_swap(int a, int b)
{
    unsigned *bag = tb_state.bag;
    int temp;
    
    temp = bag[a];
    bag[a] = bag[b];
    bag[b] = temp;
}
