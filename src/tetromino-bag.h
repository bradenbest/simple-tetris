#ifndef TETROMINO_BAG_H
#define TETROMINO_BAG_H

#include "tetromino.h"

struct tetromino  *  tetromino_bag_get  (void);

#endif
