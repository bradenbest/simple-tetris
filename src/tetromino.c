#include "tetromino.h"
#include "defs.h"
#include "coord.h"
#include "ground.h"

#include <string.h>

static int  bounds_check_rotation  (struct tetromino *);
static int  bounds_check_right     (struct tetromino *);
static int  bounds_check_left      (struct tetromino *);
static int  bounds_check_down      (struct tetromino *);
static int  bounds_check_up        (struct tetromino *);

struct coord *
tetromino_get_sprite(struct tetromino *piece)
{
    if(piece == NULL)
        return NULL;

    return piece->sprite + piece->rotation * 4;
}

struct coord *
tetromino_get_absolute_position(struct tetromino *piece)
{
    static struct coord out[4];
    struct coord *selsprite = tetromino_get_sprite(piece);

    for(int i = 0; i < 4; i++)
        coord_set(out + i, piece->position.x + selsprite[i].x, piece->position.y + selsprite[i].y);

    return out;
}

void
tetromino_reset(struct tetromino *piece)
{
    coord_set(&piece->position, 3, 0);
    piece->rotation = 0;
}

int
tetromino_swap(struct tetromino *ta, struct tetromino *tb)
{
    struct tetromino temp;
    size_t size = sizeof (struct tetromino);

    if(ta == NULL || tb == NULL)
        return 0;

    memcpy(&temp, ta,    size);
    memcpy(ta,    tb,    size);
    memcpy(tb,    &temp, size);

    return 1;
}

int
tetromino_slam_down(struct tetromino *piece)
{
    while(tetromino_move_down(piece))
        ;

    return 1;
}

int
tetromino_rotate_r(struct tetromino *piece)
{
    int old_rotation = piece->rotation;

    piece->rotation += 1;
    piece->rotation %= piece->period;

    if(!bounds_check_rotation(piece)){
        piece->rotation = old_rotation;
        return 0;
    }

    return 1;
}

int
tetromino_rotate_l(struct tetromino *piece)
{
    int old_rotation = piece->rotation;

    piece->rotation += piece->period - 1;
    piece->rotation %= piece->period;

    if(!bounds_check_rotation(piece)){
        piece->rotation = old_rotation;
        return 0;
    }

    return 1;
}

int
tetromino_move_right(struct tetromino *piece)
{
    piece->position.x++;

    if(bounds_check_right(piece)){
        piece->position.x--;
        return 0;
    }

    return 1;
}

int
tetromino_move_left(struct tetromino *piece)
{
    piece->position.x--;

    if(bounds_check_left(piece)){
        piece->position.x++;
        return 0;
    }

    return 1;
}

int
tetromino_move_down(struct tetromino *piece)
{
    piece->position.y++;

    if(bounds_check_down(piece)){
        piece->position.y--;
        return 0;
    }

    return 1;
}

int
tetromino_move_up(struct tetromino *piece)
{
    piece->position.y--;

    if(bounds_check_up(piece)){
        piece->position.y++;
        return 0;
    }

    return 1;
}

static int
bounds_check_rotation(struct tetromino *piece)
{
    while(bounds_check_right(piece))
        if(!tetromino_move_left(piece))
            return 0;

    while(bounds_check_left(piece))
        if(!tetromino_move_right(piece))
            return 0;

    while(bounds_check_down(piece))
        if(!tetromino_move_up(piece))
            return 0;

    return 1;
}

static int
bounds_check_right(struct tetromino *piece)
{
    struct coord *positions = tetromino_get_absolute_position(piece);

    if(ground_collision_check(positions))
        return 2;

    for(int i = 0; i < 4; i++)
        if(positions[i].x >= STAGE_WIDTH)
            return 1;

    return 0;
}

static int
bounds_check_left(struct tetromino *piece)
{
    struct coord *positions = tetromino_get_absolute_position(piece);

    if(ground_collision_check(positions))
        return 2;

    for(int i = 0; i < 4; i++)
        if(positions[i].x < 0)
            return 1;

    return 0;
}

static int
bounds_check_down(struct tetromino *piece)
{
    struct coord *positions = tetromino_get_absolute_position(piece);

    if(ground_collision_check(positions))
        return 2;

    for(int i = 0; i < 4; i++)
        if(positions[i].y >= STAGE_HEIGHT)
            return 1;

    return 0;
}

static int
bounds_check_up(struct tetromino *piece)
{
    struct coord *positions = tetromino_get_absolute_position(piece);

    if(ground_collision_check(positions))
        return 2;

    return 0;
}
