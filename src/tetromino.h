#ifndef TETROMINO_H
#define TETROMINO_H

#include "defs.h"
#include "coord.h"

struct tetromino {
    struct coord position;
    struct coord sprite[16];
    int color;
    int period;
    int rotation;
};

struct coord  *  tetromino_get_sprite             (struct tetromino *);
struct coord  *  tetromino_get_absolute_position  (struct tetromino *);
void             tetromino_reset                  (struct tetromino *);
int              tetromino_swap                   (struct tetromino *, struct tetromino *);
int              tetromino_slam_down              (struct tetromino *);
int              tetromino_rotate_r               (struct tetromino *);
int              tetromino_rotate_l               (struct tetromino *);
int              tetromino_move_right             (struct tetromino *);
int              tetromino_move_left              (struct tetromino *);
int              tetromino_move_down              (struct tetromino *);
int              tetromino_move_up                (struct tetromino *);

#endif
