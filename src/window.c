#include "window.h"

#include <ncurses.h>

void
window_move_cursor(struct window *window, int x, int y)
{
    window->cursor.x = x;
    window->cursor.y = y;
    window_move_real_cursor(window);
}

void
window_move_real_cursor(struct window *window)
{
    move(window->pos.y + window->cursor.y, (window->pos.x + window->cursor.x) * 2);
}
