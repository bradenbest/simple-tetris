#ifndef WINDOW_H
#define WINDOW_H

#include "coord.h"

#include <stdbool.h>

struct window {
    struct coord pos;
    struct coord size;
    struct coord cursor;
    char *title;
    bool border;
};

void  window_move_cursor       (struct window *window, int x, int y);
void  window_move_real_cursor  (struct window *window);

#endif
